import com.google.protobuf.gradle.protobuf
import com.google.protobuf.gradle.protoc

group = "BoostedKafkaHandsOn"
version = "1.0-SNAPSHOT"

plugins {
    java
    application
    id("org.springframework.boot") version "2.4.5"
    id("io.freefair.lombok") version "5.3.0"
    id("org.openapi.generator") version "4.3.1"
    id("io.qameta.allure") version "2.8.1"
    id("com.google.protobuf") version "0.8.16"
    id("com.github.davidmc24.gradle.plugin.avro") version "1.2.0"
}

group = "test"
version = "1.0.0"

configure<JavaPluginConvention> {
    sourceCompatibility = JavaVersion.VERSION_11
    targetCompatibility = JavaVersion.VERSION_11
}

repositories {
    maven {
        url = uri("http://registry.tcsbank.ru/repository/mvn-maven-proxy/")
    }
    maven {
        url = uri("http://packages.confluent.io/maven/")
    }
    maven {
        url = uri("http://nexus-new.tcsbank.ru/content/repositories/mvn-invest-sdet-releases/")
    }
    mavenCentral()
}

sourceSets {
    test {
        java {
            srcDir("$buildDir/generated/source/proto/test/java")
        }
        proto {
            srcDir("src/test/java/kafka/proto")
        }
    }
}

protobuf {
    protoc {
        // The artifact spec for the Protobuf Compiler
        artifact = "com.google.protobuf:protoc:3.14.0"
    }
}

allure {
    version = "2.13.7"
    autoconfigure = true
    aspectjweaver = false
}

dependencies {
    val allureVersion = "2.13.7"
    val springVersion = "2.4.5"

    testImplementation("org.springframework.boot:spring-boot-starter-test:$springVersion") {
        exclude("junit", "junit")
        exclude("org.junit.vintage", "junit-vintage-engine")
    }
    implementation("org.springframework.boot:spring-boot-starter-data-jpa:$springVersion")
    implementation("org.springframework.boot:spring-boot-starter-json:$springVersion")
    implementation("org.springframework.kafka:spring-kafka:2.8.0")

    // Allure
    implementation("io.qameta.allure:allure-rest-assured:$allureVersion")
    implementation("io.qameta.allure:allure-junit5:$allureVersion")

    implementation("io.confluent:kafka-avro-serializer:6.0.1")
    implementation("io.confluent:kafka-protobuf-serializer:6.0.1")
    implementation("org.apache.avro:avro:1.10.1")
    implementation("com.google.protobuf:protobuf-java:3.14.0")
    implementation("ru.tinkoff.invest.sdet:boostedkafka:1.2.0")

    implementation("javax.xml.bind:jaxb-api:2.3.1")
    implementation("org.apache.commons:commons-lang3:3.12.0")

    testImplementation("org.junit.jupiter:junit-jupiter:5.7.1")

    // Async waiters
    implementation("org.awaitility:awaitility:4.0.3")
}

tasks.test {
    useJUnitPlatform()
}

tasks.generateAvroProtocol {
    source("src/test/java/kafka/avro")
}

tasks.generateAvroJava {
    source("src/test/java/kafka/avro")
}

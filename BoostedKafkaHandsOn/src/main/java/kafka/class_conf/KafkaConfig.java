package kafka.class_conf;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.jetbrains.annotations.NotNull;
import ru.tinkoff.invest.sdet.kafka.prototype.reciever.BoostedReceiverImpl;
import ru.tinkoff.invest.sdet.kafka.prototype.sender.BoostedSenderImpl;

import java.util.Properties;

public class KafkaConfig {

    @NotNull
    public static BoostedReceiverImpl<String, byte[]> getReceiver() {
        Properties consumerConfig = new Properties();
        consumerConfig.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "vm-kafka-stp01t.tcsbank.ru:9092");
        consumerConfig.put(ConsumerConfig.GROUP_ID_CONFIG, "protobuf-group");
        BoostedReceiverImpl<String, byte[]> receiver = new BoostedReceiverImpl<>(consumerConfig) {};
        return receiver;
    }

    @NotNull
    public static BoostedSenderImpl<String, byte[]> getSender() {
        Properties freshConfig = new Properties();
        freshConfig.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "vm-kafka-stp01t.tcsbank.ru:9092");
        freshConfig.put("schema.registry.url", "http://vm-kafka-stp01t.tcsbank.ru:8081");
        freshConfig.put("acks", "all");
        freshConfig.put("request.timeout.ms", "60000");
        BoostedSenderImpl<String, byte[]> s = new BoostedSenderImpl<>(freshConfig) {};
        return s;
    }
}

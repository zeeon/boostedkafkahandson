package kafka.spring_conf.services;

import com.google.protobuf.GeneratedMessageV3;
import io.qameta.allure.Step;
import kafka.spring_conf.topic.Topics;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.tinkoff.invest.sdet.kafka.prototype.reciever.BoostedReceiver;

import java.time.Duration;
import java.util.List;
import java.util.stream.Collectors;

import static org.awaitility.Awaitility.await;
import static utils.AllureUtils.FOUND_ENTITIES;
import static utils.AllureUtils.addTextAttachment;


@Slf4j
@Service
@RequiredArgsConstructor
public class ProtobufReceiverService<V extends GeneratedMessageV3> {

    private final BoostedReceiver<String, byte[]> receiver;



    @SneakyThrows
    @Step("Получить сообщения из Kafka топика {topic.name}")
    public List<V> receiveBatch(Topics topic, Duration duration, Class<V> messageClass) {
        String topicName = topic.getName();
        log.info("Получен запрос на получение из Kafka топика {} сообщений {}", topicName,
            messageClass.getName());
        List<V> result = receiver.receiveBatch(topicName, duration).stream()
            .map(x -> parse(x, messageClass)).collect(Collectors.toList());
        log.info("Прочитано из {} сообщений: {}", topicName, result.size());
        addTextAttachment(FOUND_ENTITIES, result);
        return result;
    }

    @SneakyThrows
    private V parse(byte[] value, Class<V> messageClass) {
        var parseMethod = messageClass.getMethod("parseFrom", byte[].class);
        return (V) parseMethod.invoke(null, value);
    }
}

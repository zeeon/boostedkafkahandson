package kafka.spring_conf.services;

import com.google.protobuf.GeneratedMessageV3;
import io.qameta.allure.Step;
import kafka.spring_conf.topic.Topics;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.tinkoff.invest.sdet.kafka.prototype.sender.BoostedSender;

import java.util.List;

import static utils.AllureUtils.addTextAttachment;


@Slf4j
@Service
@RequiredArgsConstructor
public class ProtobufSenderService<V extends GeneratedMessageV3> {

    private final BoostedSender<String, byte[]> sender;

    @Step("Отправить сообщение в Kafka топик {topic.name}")
    public void send(Topics topic, String key, V msg) {
        String topicName = topic.getName();
        log.info("Полечен запрос на отправку в Kafka {} сообщения {}", topicName,
            msg.getClass().getName());
        sender.send(topicName, key, msg.toByteArray());
        log.info("Отправка в Kafka {} сообщения {} успешно завершена", topicName,
            msg.getClass().getName());
        addTextAttachment("Сообщение", msg);
    }
}
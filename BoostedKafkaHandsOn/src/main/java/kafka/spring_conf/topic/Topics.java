package kafka.spring_conf.topic;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;

@Getter
@Accessors(chain = true)
@RequiredArgsConstructor
public enum Topics {
    TEST("qa3.MLIM");

    private final String name;
}

package kafka.spring_conf.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;


@Data
@Validated
@ConfigurationProperties(prefix = "app.kafka")
public class KafkaConfigurationProperties {

    private String servers;
    private String schemaRegistryUrl;
}

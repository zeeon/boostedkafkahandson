package kafka.spring_conf.configuration;

import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.*;
import ru.tinkoff.invest.sdet.kafka.prototype.reciever.BoostedReceiver;
import ru.tinkoff.invest.sdet.kafka.prototype.reciever.BoostedReceiverImpl;
import ru.tinkoff.invest.sdet.kafka.prototype.sender.BoostedSender;
import ru.tinkoff.invest.sdet.kafka.prototype.sender.BoostedSenderImpl;

import java.util.Properties;

@Configuration
@ComponentScan("kafka.spring_conf.services")
@Import({JacksonAutoConfiguration.class})
@EnableConfigurationProperties(KafkaConfigurationProperties.class)
public class KafkaAutoConfiguration {

    @Bean
    @Primary
    public Properties kafkaProperties(KafkaConfigurationProperties config) {
        var props = new Properties();
        props.put("bootstrap.servers", config.getServers());
        props.put("acks", "all");
        props.put("group.id", "protobuf-group");
        props.put("enable.auto.commit", "false");
        return props;
    }

    @Bean
    public BoostedSender<String, String> stringSender(Properties kafkaProperties) {
        return new BoostedSenderImpl<>(kafkaProperties) {
        };
    }

    @Bean
    public BoostedReceiver<String, String> stringReceiver(Properties kafkaProperties) {
        return new BoostedReceiverImpl<>(kafkaProperties) {
        };
    }
    @Bean
    public BoostedSender<String, byte[]> byteSender(Properties kafkaProperties) {
        return new BoostedSenderImpl<>(kafkaProperties) {
        };
    }

    @Bean
    public BoostedReceiver<String, byte[]> byteReceiver(Properties kafkaProperties) {
        return new BoostedReceiverImpl<>(kafkaProperties) {
        };
    }
}

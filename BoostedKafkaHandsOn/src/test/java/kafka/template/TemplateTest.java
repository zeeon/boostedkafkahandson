package kafka.template;

import com.google.protobuf.InvalidProtocolBufferException;
import io.qameta.allure.junit5.AllureJunit5;
import old_kafka.template.KafkaHelper;
import old_kafka.template.KafkaMessageConsumer;
import org.apache.kafka.common.serialization.ByteArrayDeserializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.kafka.core.KafkaTemplate;
import ru.tinkoff.invest.sdet.kafka.prototype.reciever.BoostedReceiverImpl;
import ru.tinkoff.invest.sdet.kafka.prototype.sender.BoostedSenderImpl;
import ru.tinkoff.invest.sdet.proto.Dog;
import ru.tinkoff.invest.sdet.proto.DogId;

import java.time.Duration;
import java.time.Instant;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

@ExtendWith({AllureJunit5.class})
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class TemplateTest {
    KafkaHelper kafkaHelper = new KafkaHelper();

    @Test
    void templateTest() throws InterruptedException {
        KafkaTemplate<String, String> stringToStringTemplate = kafkaHelper.createStringToStringTemplate();
        stringToStringTemplate.send("qa3.MLIM", "Hello", "Value");
        stringToStringTemplate.flush();

        try (KafkaMessageConsumer<String, String> messageConsumer =
                 new KafkaMessageConsumer<>(kafkaHelper, "qa3.MLIM",
                     StringDeserializer.class, StringDeserializer.class)) {
            messageConsumer.startUp();
            //смотрим, сообщение, которое поймали в топике kafka
            KafkaMessageConsumer.Record<String, String> record = messageConsumer.await()
                .orElseThrow(() -> new RuntimeException("Сообщение не получено"));
            Thread.sleep(5000);
            List<KafkaMessageConsumer.Record<String, String>> records = messageConsumer.listRecords();
            System.out.println(records.size());
            System.out.println(records.get(0).key);
            System.out.println(records.get(0).value);
        }
    }
}
package kafka;

import com.google.protobuf.InvalidProtocolBufferException;
import io.qameta.allure.junit5.AllureJunit5;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import ru.tinkoff.invest.sdet.kafka.prototype.reciever.BoostedReceiverImpl;
import ru.tinkoff.invest.sdet.kafka.prototype.sender.BoostedSenderImpl;
import ru.tinkoff.invest.sdet.proto.Dog;
import ru.tinkoff.invest.sdet.proto.DogId;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

@ExtendWith({AllureJunit5.class})
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class KafkaTestWithInPlaceConf {

    @Test
    void protobufTest() {
        BoostedSenderImpl<String, byte[]> s = getSender();
        s.send("qa3.MLIM", DogId.newBuilder().setId("1").toString(), Dog.newBuilder().setName("Sharik").setColor("blue").build().toByteArray());

        BoostedReceiverImpl<String, byte[]> receiver = getReceiver();
        List<byte[]> bytes = receiver.receiveBatch("qa3.MLIM", Duration.ofSeconds(10));
        List<Dog> events = new ArrayList<>();
        bytes.forEach(b -> {
            try {
                events.add(Dog.parseFrom(b));
            } catch (InvalidProtocolBufferException e) {
                e.printStackTrace();
            }
        });
        System.out.println(events);
    }

    @NotNull
    private BoostedReceiverImpl<String, byte[]> getReceiver() {
        Properties consumerConfig = new Properties();
        consumerConfig.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "vm-kafka-stp01t.tcsbank.ru:9092");
        consumerConfig.put(ConsumerConfig.GROUP_ID_CONFIG, "protobuf-group");
        BoostedReceiverImpl<String, byte[]> receiver = new BoostedReceiverImpl<>(consumerConfig) {};
        return receiver;
    }

    @NotNull
    private BoostedSenderImpl<String, byte[]> getSender() {
        Properties freshConfig = new Properties();
        freshConfig.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "vm-kafka-stp01t.tcsbank.ru:9092");
        freshConfig.put("schema.registry.url", "http://vm-kafka-stp01t.tcsbank.ru:8081");
        freshConfig.put("acks", "all");
        freshConfig.put("request.timeout.ms", "60000");
        BoostedSenderImpl<String, byte[]> s = new BoostedSenderImpl<>(freshConfig) {};
        return s;
    }
}

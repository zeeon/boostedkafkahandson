package kafka;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.protobuf.InvalidProtocolBufferException;
import io.qameta.allure.junit5.AllureJunit5;
import kafka.json.Car;
import kafka.xml.Student;
import lombok.SneakyThrows;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import ru.tinkoff.invest.sdet.avro.Cat;
import ru.tinkoff.invest.sdet.kafka.prototype.reciever.BoostedReceiverImpl;
import ru.tinkoff.invest.sdet.kafka.prototype.reciever.dto.GenericMessage;
import ru.tinkoff.invest.sdet.kafka.prototype.sender.BoostedSenderImpl;
import ru.tinkoff.invest.sdet.proto.Dog;
import ru.tinkoff.invest.sdet.proto.DogId;

import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.TimeZone;

@ExtendWith({AllureJunit5.class})
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class KafkaTestWithoutConfiguration {

    @Test
    void protobufTest() {
        Properties freshConfig = new Properties();
        freshConfig.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "vm-kafka-stp01t.tcsbank.ru:9092");
        freshConfig.put("schema.registry.url", "http://vm-kafka-stp01t.tcsbank.ru:8081");
        freshConfig.put("acks", "all");
        freshConfig.put("request.timeout.ms", "60000");
        BoostedSenderImpl<String, byte[]> s = new BoostedSenderImpl<>(freshConfig) {};
        s.send("qa3.MLIM", DogId.newBuilder().setId("1").toString(), Dog.newBuilder().setName("Sharik").setColor("blue").build().toByteArray());
        Properties consumerConfig = new Properties();

        consumerConfig.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "vm-kafka-stp01t.tcsbank.ru:9092");
        consumerConfig.put(ConsumerConfig.GROUP_ID_CONFIG, "protobuf-group");
        BoostedReceiverImpl<String, byte[]> receiver = new BoostedReceiverImpl<>(consumerConfig) {};
        List<byte[]> bytes = receiver.receiveBatch("qa3.MLIM", Duration.ofSeconds(10));
        List<Dog> events = new ArrayList<>();
        bytes.forEach(b -> {
            try {
                events.add(Dog.parseFrom(b));
            } catch (InvalidProtocolBufferException e) {
                e.printStackTrace();
            }
        });
        System.out.println(events);
    }

    @Test
    void StringTest() {
        Properties freshConfig = new Properties();
        freshConfig.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "vm-kafka-stp01t.tcsbank.ru:9092");
        freshConfig.put("schema.registry.url", "http://vm-kafka-stp01t.tcsbank.ru:8081");
        freshConfig.put("acks", "all");
        freshConfig.put("request.timeout.ms", "60000");

        BoostedSenderImpl<String,String> s = new BoostedSenderImpl<>(freshConfig) {};
        s.send("qa3.MLIM", "Тестовое сообщение");

        Properties consumerConfig = new Properties();
        consumerConfig.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "vm-kafka-stp01t.tcsbank.ru:9092");
        consumerConfig.put(ConsumerConfig.GROUP_ID_CONFIG, "protobuf-group");
        BoostedReceiverImpl<String, String> receiver = new BoostedReceiverImpl<>(consumerConfig) {};

        List<String> strings = receiver.receiveBatch("qa3.MLIM", Duration.ofSeconds(10));
        System.out.println(strings);
    }

    @Test
    void bytesTest() {
        Properties freshConfig = new Properties();
        freshConfig.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "vm-kafka-stp01t.tcsbank.ru:9092");
        freshConfig.put("schema.registry.url", "http://vm-kafka-stp01t.tcsbank.ru:8081");
        freshConfig.put("acks", "all");
        freshConfig.put("request.timeout.ms", "60000");

        BoostedSenderImpl<String, byte[]> s = new BoostedSenderImpl<>(freshConfig) {};
        s.send("qa3.MLIM", "Тестовое сообщение".getBytes(StandardCharsets.UTF_8));

        Properties consumerConfig = new Properties();
        consumerConfig.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "vm-kafka-stp01t.tcsbank.ru:9092");
        consumerConfig.put(ConsumerConfig.GROUP_ID_CONFIG, "protobuf-group");
        BoostedReceiverImpl<String, byte[]> receiver = new BoostedReceiverImpl<>(consumerConfig) {};

        List<byte[]> bytes = receiver.receiveBatch("qa3.MLIM", Duration.ofSeconds(10));
        List<String> events = new ArrayList<>();
        bytes.forEach(b -> {
            try {
                events.add(new String(b));
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        System.out.println(events);
    }

    @Test
    void avroTest() {
        Properties freshConfig = new Properties();
        freshConfig.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "vm-kafka-stp01t.tcsbank.ru:9092");
        freshConfig.put("schema.registry.url", "http://vm-kafka-stp01t.tcsbank.ru:8081");
        freshConfig.put("acks", "all");

        BoostedSenderImpl<String, Cat> s = new BoostedSenderImpl<>(freshConfig) {};
        s.send("qa3.MLIM", Cat.newBuilder().setName("Vasya").setColor("Striped").build());

        Properties consumerConfig = new Properties();
        consumerConfig.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "vm-kafka-stp01t.tcsbank.ru:9092");
        consumerConfig.put(ConsumerConfig.GROUP_ID_CONFIG, "avro-group");
        BoostedReceiverImpl<String, Cat> receiver = new BoostedReceiverImpl<>(consumerConfig) {};

        List<Cat> events = receiver.receiveBatch("qa3.MLIM", Duration.ofSeconds(10));
        System.out.println(events);
    }

    @Test
    void xmlTest() {
        Properties freshConfig = new Properties();
        freshConfig.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "vm-kafka-stp01t.tcsbank.ru:9092");
        BoostedSenderImpl<String, Student> s = new BoostedSenderImpl<String, Student>(freshConfig) {
        };
        s.send("qa3.MLIM", Student.builder().age(10).name("Vasya").build());

        Properties consumerConfig = new Properties();
        consumerConfig.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "vm-kafka-stp01t.tcsbank.ru:9092");
        consumerConfig.put(ConsumerConfig.GROUP_ID_CONFIG, "protobuf-group");
        BoostedReceiverImpl<String, Student> receiver = new BoostedReceiverImpl<String, Student>(consumerConfig) {
        };
        List<Student> students = receiver.receiveBatch("qa3.MLIM");
        System.out.println(students);
    }

    @Test
    @SneakyThrows
    void jsonTest() {
        Properties freshConfig = new Properties();
        freshConfig.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "vm-kafka-stp01t.tcsbank.ru:9092");
        BoostedSenderImpl<String, Car> s = new BoostedSenderImpl<String, Car>(freshConfig) {
        };
        Car car = Car.builder().color("Red").type("BMW").build();
        s.send("qa3.MLIM", car);

        Properties consumerConfig = new Properties();
        consumerConfig.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "vm-kafka-stp01t.tcsbank.ru:9092");
        consumerConfig.put(ConsumerConfig.GROUP_ID_CONFIG, "protobuf-group");
        BoostedReceiverImpl<String, Car> receiver = new BoostedReceiverImpl<>(consumerConfig) {
        };
        List<Car> strings = receiver.receiveBatch("qa3.MLIM", Duration.ofSeconds(10));
        System.out.println(strings);
    }
}

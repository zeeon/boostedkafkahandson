package kafka.json;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import ru.tinkoff.invest.sdet.kafka.prototype.serializer.KafkaJsonMessage;

@Data
@Builder
@AllArgsConstructor
public class Car implements KafkaJsonMessage {
    private String color;
    private String type;

    public Car() {
    }
}

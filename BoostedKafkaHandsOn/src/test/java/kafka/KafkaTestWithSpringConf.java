package kafka;

import com.google.protobuf.InvalidProtocolBufferException;
import io.qameta.allure.junit5.AllureJunit5;
import kafka.spring_conf.configuration.KafkaAutoConfiguration;
import kafka.spring_conf.services.ProtobufReceiverService;
import kafka.spring_conf.services.ProtobufSenderService;
import kafka.spring_conf.topic.Topics;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import ru.tinkoff.invest.sdet.kafka.prototype.reciever.BoostedReceiverImpl;
import ru.tinkoff.invest.sdet.kafka.prototype.sender.BoostedSenderImpl;
import ru.tinkoff.invest.sdet.proto.Dog;
import ru.tinkoff.invest.sdet.proto.DogId;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

import static kafka.class_conf.KafkaConfig.getReceiver;

@ExtendWith({AllureJunit5.class})
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@SpringBootTest(classes = {KafkaAutoConfiguration.class})
public class KafkaTestWithSpringConf {

    @Autowired
    ProtobufSenderService<Dog> sender;
    @Autowired
    ProtobufReceiverService<Dog> receiver;

    @Test
    void protobufTest() {
        String key = DogId.newBuilder().setId("1").toString();
        Dog message = Dog.newBuilder().setName("Sharik").setColor("blue").build();
        sender.send(Topics.TEST, key, message);

        List<Dog> dogs = receiver.receiveBatch(Topics.TEST, Duration.ofSeconds(10), Dog.class);

        System.out.println(dogs);
    }
}

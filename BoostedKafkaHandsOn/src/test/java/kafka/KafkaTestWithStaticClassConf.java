package kafka;


import com.google.protobuf.InvalidProtocolBufferException;
import io.qameta.allure.junit5.AllureJunit5;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import ru.tinkoff.invest.sdet.kafka.prototype.reciever.BoostedReceiverImpl;
import ru.tinkoff.invest.sdet.kafka.prototype.sender.BoostedSenderImpl;
import ru.tinkoff.invest.sdet.proto.Dog;
import ru.tinkoff.invest.sdet.proto.DogId;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

import static kafka.class_conf.KafkaConfig.getReceiver;
import static kafka.class_conf.KafkaConfig.getSender;

@ExtendWith({AllureJunit5.class})
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class KafkaTestWithStaticClassConf {

    @Test
    void protobufTest() {
        BoostedSenderImpl<String, byte[]> s = getSender();
        s.send("qa3.MLIM", DogId.newBuilder().setId("1").toString(), Dog.newBuilder().setName("Sharik").setColor("blue").build().toByteArray());

        BoostedReceiverImpl<String, byte[]> receiver = getReceiver();
        List<byte[]> bytes = receiver.receiveBatch("qa3.MLIM", Duration.ofSeconds(10));
        List<Dog> events = new ArrayList<>();
        bytes.forEach(b -> {
            try {
                events.add(Dog.parseFrom(b));
            } catch (InvalidProtocolBufferException e) {
                e.printStackTrace();
            }
        });
        System.out.println(events);
    }
}

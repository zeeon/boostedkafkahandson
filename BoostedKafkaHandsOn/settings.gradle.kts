rootProject.name = "BoostedKafkaHandsOn"

pluginManagement {
    repositories {
        maven {
            name = "JCenter Gradle Plugins"
            url = uri("https://dl.bintray.com/gradle/gradle-plugins")
        }
        gradlePluginPortal()
        jcenter()
    }
}
